'use strict';

const express = require('express');
const app = express();
const path = require('path');
const Specification = require('@atomos/specification');

app.set('port', process.env.PORT || 3000);

app.use(express.static(path.resolve('dist')));

app.get('/*', (req, res) => {
	res.sendFile(path.resolve('dist/index.html'));
});

// consume private @atomos package
const hasCorrectPort = new Specification(port => port === (process.env.PORT || 3000));
if (!hasCorrectPort.describes(app.get('port'))) throw Error('This app has not sort the appropriate port');

app.listen(app.get('port'));
