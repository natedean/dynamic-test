import Types from './actionTypes';
import { NOTIFICATION_TIMEOUT } from '../constants/notification';

let timeout;

/**
 * @function showNotification
 * @return {thunk} A thunk responsible for showing the setting state.isNotifying
 * @description
 * Dispatches clearNotification thunk if needed.  Dispatches show notification action.  Sets timeout to dispatch
 * 	a hide notification action.
 * */
const showNotification = () => (dispatch, getState) => {
	if (getState().isNotifying) dispatch(clearNotification());
	dispatch({
		type: Types.SHOW_NOTIFICATION
	});

	timeout = setTimeout(() => {
		dispatch({
			type: Types.HIDE_NOTIFICATION
		});
	}, NOTIFICATION_TIMEOUT);
};

/**
 * @function clearNotification
 * @return {thunk} A thunk responsible for clearing the notification timeout and resetting state.isNotifying
 * @description
 * Clear timeout. Dispatch hide notification action.
 * */
const clearNotification = () => (dispatch) => {
	clearTimeout(timeout);
	dispatch({
		type: Types.HIDE_NOTIFICATION
	});
};

export default showNotification;
