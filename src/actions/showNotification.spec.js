import test from 'tape';
import showNotification from './showNotification';
import initialAppState from '../reducers/initialAppState';
import types from './actionTypes';

test(`${types.SHOW_NOTIFICATION}; If no notification is showing, the notification should toggle on, then off`, (t) => {
	let hasShownNotification = false;
	// ARRANGE
	const store = {
		getState: () => (initialAppState),
		dispatch: (action) => {
			// this thunk should dispatch an action to SHOW_NOTIFICATION, then after a time dispatch another action
			// to HIDE_NOTIFICATION
			if (!hasShownNotification) {
				t.equal(action.type, types.SHOW_NOTIFICATION);
				hasShownNotification = true;
			} else {
				t.equal(action.type, types.HIDE_NOTIFICATION);
			}
		}
	};

	// ACT
	showNotification()(store.dispatch, store.getState);

	t.end();
});
