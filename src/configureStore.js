import {createStore, applyMiddleware} from 'redux';
import rootReducer from './reducers';
import thunk from 'redux-thunk';
import createLogger from 'redux-logger';
import {composeWithDevTools} from 'redux-devtools-extension/developmentOnly';
import injectTapEventPlugin from 'react-tap-event-plugin';

// Facebook's TapEventPlugin allows access to synthetic DOM events! Really sweet stuff.
injectTapEventPlugin();

const loggerMiddleware = createLogger();

// Be sure to ONLY add this middleware in development!
const middleware = process.env.NODE_ENV !== 'production' ?
	[require('redux-immutable-state-invariant')(), thunk, loggerMiddleware] :
	[thunk];

// Be sure to ONLY add this composeWithDevTools in development!
const composeEnhancers = process.env.NODE_ENV !== 'production' ?
	composeWithDevTools(applyMiddleware(...middleware)) :
	applyMiddleware(...middleware);

export default function configureStore(initialState) {
	return createStore(
		rootReducer,
		initialState,
		composeEnhancers
	);
}
