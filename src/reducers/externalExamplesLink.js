import initialState from './initialAppState';

const externalExamplesLink = (state = initialState.externalExamplesLink, action) => {
	switch(action.type) {
		// just a stub
		default:
			return state;
	}
};

export default externalExamplesLink;
