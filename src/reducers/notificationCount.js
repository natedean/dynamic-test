import initalAppState from './initialAppState';
import ActionTypes from '../actions/actionTypes';

const notificationCount = (state = initalAppState.notificationCount, action) => {
	switch (action.type) {
		case ActionTypes.SHOW_NOTIFICATION:
			return state + 1;
		default:
			return state;
	}
};

export default notificationCount;
