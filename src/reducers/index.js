import {combineReducers} from 'redux';
import {routerReducer as routing} from 'react-router-redux';

import isNotifying from './isNotifying';
import notificationMessage from './notificationMessage';
import externalExamplesLink from './externalExamplesLink';
import notificationCount from './notificationCount';

const rootReducer = combineReducers({
	isNotifying,
	notificationMessage,
	externalExamplesLink,
	notificationCount,
	routing
});

export default rootReducer;
