import PropTypes from 'prop-types';
import React from 'react';
import {connect} from 'react-redux';
import {browserHistory} from 'react-router';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import AppBar from 'material-ui/AppBar';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import Snackbar from 'material-ui/Snackbar';
import Col from '../components/common/Col';

const muiTheme = getMuiTheme({
	fontFamily: 'Open Sans, sans-serif'
});

/**
 * @class App
 * @param {bool} isNotifying - Boolean to either show or hide the notification message
 * @param {string} notificationMessage - Custom notification message we want to display
 * @param {node} [children] - Child node(s)
 * @classdesc
 * This is the App Container
 * */
class App extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			isDrawerOpen: false
		};

		this.toggle = this.toggle.bind(this);
		this.navigate = this.navigate.bind(this);
	}

	toggle() {
		this.setState((prevState) => ({isDrawerOpen: !prevState.isDrawerOpen}));
	}

	navigate(route, isDrawerOpen = false) {
		browserHistory.push(route);
		this.setState(() => ({ isDrawerOpen }));
	}

	render() {
		const { isNotifying, notificationMessage, children } = this.props;

		return (
			<MuiThemeProvider muiTheme={muiTheme}>
				<main>
					<AppBar
						title="Boiler plate"
						iconElementLeft={
							<IconButton onTouchTap={this.toggle}>
								<MoreVertIcon />
							</IconButton>
						}
					/>
					<Drawer
						open={this.state.isDrawerOpen}
						docked={false}
						onRequestChange={this.toggle}>
						<MenuItem onClick={() => this.navigate('/', false)}>Home</MenuItem>
						<MenuItem onClick={() => this.navigate('/reduxExample', false, false)}>Redux Example</MenuItem>
					</Drawer>
					<div className="container">
						<Col xs="12">
							{children}
						</Col>
					</div>
					<Snackbar
						message={notificationMessage}
						open={isNotifying}
					/>
				</main>
			</MuiThemeProvider>
		);
	}
}

const mapStateToProps = state => ({
	isNotifying: state.isNotifying,
	notificationMessage: `${state.notificationMessage} ${state.notificationCount}x`
});

App.propTypes = {
	isNotifying: PropTypes.bool.isRequired,
	notificationMessage: PropTypes.string.isRequired,
	children: PropTypes.node
};

export default connect(mapStateToProps)(App);
