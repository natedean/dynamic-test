import React from 'react';
import {Route, IndexRoute} from 'react-router';

import AppContainer from './containers/App';
import ReduxExampleContainer from './containers/ReduxExample';
import Home from './components/home/Home';

/**
 * Note that currently in order for HMR (Hot Module Reloading) to work correctly,
 * your higher order components must be React Components NOT Stateless Functions,
 * hence AppContainer is a Component.
 *
 * Notice, however that Home is a Stateless Function, and how props can still be passed in.
 */
export default (
	<Route path="/" component={AppContainer}>
		<IndexRoute component={Home}/>
		<Route
			path="/reduxExample"
			component={ReduxExampleContainer}
		/>
	</Route>
);
