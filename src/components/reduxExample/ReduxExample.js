import PropTypes from 'prop-types';
import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import Col from '../common/Col';
import Row from '../common/Row';

/**
 * This is now just a stateless function.
 *
 *  -- If you do not need access to state then this approach has a couple advantages --
 *  1.) You don't need to bother extending the React.Component class which means lower memory footprint
 *  2.) This is now easier to test, you can only pass props in and display components, limits the potential for failure
 *      since there is no lifecycle.
 */

/**
 * @function ReduxExample
 * @param {function} showNotification - Example button click handler
 * @param {string} externalExampleLink - Link to show more redux examples
 * @description
 * Demonstrates a simple redux action
 * @return {object} - jsx
 */
const ReduxExample = ({showNotification, externalExamplesLink, notificationCount}) => {
	return (
		<section className="reduxExample">
			<RaisedButton
				className="reduxExample__btn"
				label="Click the button to fire off a Redux action"
				primary={true}
				onClick={showNotification}
			/>
			<p>{`You've clicked the button ${notificationCount} times.`}</p>
			<p>There are more examples <a href={externalExamplesLink} target="_blank">here</a>.
			</p>
		</section>
	);
};

/** You can still check types with stateless functions! */
ReduxExample.propTypes = {
	showNotification: PropTypes.func.isRequired,
	externalExamplesLink: PropTypes.string.isRequired,
	notificationCount: PropTypes.number.isRequired
};

export default ReduxExample;
