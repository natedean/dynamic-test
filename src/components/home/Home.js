import PropTypes from 'prop-types';
import React from 'react';
import Col from '../common/Col';
import Row from '../common/Row';

/**
 * This is now just a stateless function.
 *
 *  -- If you do not need access to state then this approach has a couple advantages --
 *  1.) You don't need to bother extending the React.Component class which means lower memory footprint
 *  2.) This is now easier to test, you can only pass props in and display components, limits the potential for failure
 *      since there is no lifecycle.
 */

/**
 * @function Home
 * @description
 * Entry page to the app
 * @return {object} - jsx
 */
const Home = () => {
	return (
		<section className="home">
			<Row>
				<Col xs="12" sm="6">
					<header className="home__title">
						React and Redux for the win!
					</header>
				</Col>
				<Col xs="12" sm="6">
					<p>React is a Javascript library for building user interfaces.</p>
					<p>Redux is a predictable state container for JavaScript apps.</p>
				</Col>
			</Row>
		</section>
	);
};

Home.propTypes = {
	rowBackgroundColor: PropTypes.string
};

export default Home;
