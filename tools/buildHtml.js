import fs from 'fs-extra';
import cheerio from 'cheerio';
import colors from 'colors';

/* eslint-disable no-console */
/* eslint-disable no-console */
/**
 * @description
 * Copy selectquote shortcut icon into dist folder
 */
fs.copy('src/images/selectquote.ico', 'dist/selectquote.ico', function (err) {
	if (err) return console.error(err);
	console.log('selectquote.ico written to /dist'.green);
});

/**
 * @description
 * Copy index file from src into dist
 * Inject stylesheet into the head of index.html
 * Inject shortcut icon into head of index.html
 */
fs.readFile('src/index.html', 'utf8', (err, markup) => {
	if (err) {
		return console.log(err.red);
	}

	const $ = cheerio.load(markup);
	const $head = $('head');
	$head.prepend('<link rel="stylesheet" href="styles.css">');
	$head.prepend('<link rel="shortcut icon" href="selectquote.ico">');

	fs.writeFile('dist/index.html', $.html(), 'utf8', function (err) {
		if (err) {
			return console.log(err.red);
		}

		console.log('index.html written to /dist'.green);
	});
});
